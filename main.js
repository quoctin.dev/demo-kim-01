const BOX_SIZE = 50
const BOARD = {
  COLUMNS: 10,
  ROWS: 10,
}
const DIRECTION = {
  LEFT: 37,
  UP: 38,
  RIGHT: 39,
  DOWN: 40,
}
const AUTO_MOVE_DELAY = 1000

let box
let currentDirection = DIRECTION.LEFT
let autoMove = false
let moveTimer

function init() {
  const board = document.getElementById('board')
  board.style.width = (BOARD.COLUMNS * BOX_SIZE) + 'px'
  board.style.height = (BOARD.ROWS * BOX_SIZE) + 'px'

  box = document.getElementById('box')
  box.style.width = box.style.height = BOX_SIZE + 'px'
}

function changeShape(event) {
  const newShape = event.target.value

  if (newShape === 'circle') {
    box.classList.add('circle')
  } else {
    box.classList.remove('circle')
  }
}

function changeBackgroundColor(event) {
  const newColor = event.target.value
  box.style.background = newColor
}

function moveBox(direction) {
  direction = direction || currentDirection

  let x = box.offsetLeft, y = box.offsetTop
  let deltaX = 0, deltaY = 0

  switch (direction) {
    case DIRECTION.UP:
      if (currentDirection === DIRECTION.DOWN) return
      deltaY = y <= 0 ? BOX_SIZE * (BOARD.ROWS - 1) : -BOX_SIZE
      break
    case DIRECTION.RIGHT:
      deltaX = BOX_SIZE
      break
    case DIRECTION.DOWN:
      if (currentDirection === DIRECTION.UP) return
      deltaY = y >= BOX_SIZE * (BOARD.ROWS - 1) ? -y : BOX_SIZE
      break
    case DIRECTION.LEFT:
      deltaX = -BOX_SIZE
      break
  }
  box.style.left = `${x + deltaX}px`
  box.style.top = `${y + deltaY}px`

  currentDirection = direction

  if (autoMove) {
    clearTimeout(moveTimer)
    moveTimer = setTimeout(moveBox, AUTO_MOVE_DELAY)
  }
}

window.onkeydown = function (event) {
  const { keyCode } = event
  
  // handle arrow buttons
  if (Object.values(DIRECTION).includes(keyCode)) {
    return moveBox(keyCode)
  }

  // handle space button
  if (keyCode === 32) {
    autoMove = !autoMove
    if (autoMove) {
      moveBox()
    }
  }
}

window.onload = function () {
  init()
}